# binance_position_info

## Description
📊 Fetches the entire trade history from your Binance spot account.

📈 Identifies open positions in your wallet and calculates the average price of the open position volume, considering all buy and sell trades.

⚙️ The algorithm is currently implemented for trades involving the USDT stablecoin, but it can be expanded to calculate for any desired asset.
 ## Settings
🔑 To connect your exchange account, provide your API keys in the api_keys.py file.
